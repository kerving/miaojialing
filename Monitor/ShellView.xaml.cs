﻿using log4net;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Threading;
using static Monitor.IPCHelper;

namespace Monitor
{
    /// <summary>
    /// ShellView.xaml 的交互逻辑
    /// </summary>
    public partial class ShellView : Window
    {
        private static readonly ILog log = LogManager.GetLogger("Monitor");

        public string MonitorSavePath
        {
            get
            {
                return ConfigurationManager.AppSettings["MonitorSavePath"] + "\\";
            }
        }

        public string PictureSavePath
        {
            get
            {
                return ((AppSettingsSection)ConfigurationManager.OpenExeConfiguration("ZXLPR.exe").GetSection("appSettings")).Settings["LPRSavePath"].Value + "\\";
            }
        }

        public ShellView()
        {
            InitializeComponent();

            Globalspace._Wnd1Handle = RealPlayWnd1.Handle;
            Globalspace._Wnd2Handle = RealPlayWnd2.Handle;
            Globalspace._Wnd3Handle = RealPlayWnd3.Handle;
            Globalspace._Wnd4Handle = RealPlayWnd4.Handle;
        }

        //由于没有标题栏，需要在整个窗口里都可以拖动窗口位置
        private void Window_DragMove(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                this.DragMove();
            }
            catch { }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            e.Cancel = true;
            base.OnClosing(e);
        }

        private void Window_Topmost(object sender, RoutedEventArgs e)
        {
            Topmost = (bool)((CheckBox)sender).IsChecked;
        }

        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);

            if (PresentationSource.FromVisual(this) is HwndSource hwndSource)
            {
                IntPtr handle = hwndSource.Handle;
                hwndSource.AddHook(new HwndSourceHook(WndProc));
            }
        }

        //通过此窗口句柄传递过来的消息
        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            try
            {

                if (msg == WM_COPYDATA)
                {
                    COPYDATASTRUCT cds = (COPYDATASTRUCT)Marshal.PtrToStructure(lParam, typeof(COPYDATASTRUCT)); // 接收封装的消息
                    string[] recv = cds.lpData.Split(':'); // 获取消息内容

                    var picPath = PictureSavePath + "/" + recv[1] + "/" + recv[2];
                    if (!System.IO.Directory.Exists(picPath))
                    {
                        System.IO.Directory.CreateDirectory(picPath);
                    }

                    // 自定义行为       
                    if (recv[0] == "FirstWeighing")
                    {
                        //if (Globalspace._UserID1 != -1) ScreenShot(Globalspace._UserID1, recv[1] + "/" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_W1_M1.jpg");
                        //if (Globalspace._UserID2 != -1) ScreenShot(Globalspace._UserID2, recv[1] + "/" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_W1_M2.jpg");
                        //if (Globalspace._UserID3 != -1) ScreenShot(Globalspace._UserID3, recv[1] + "/" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_W1_M3.jpg");
                        //if (Globalspace._UserID4 != -1) ScreenShot(Globalspace._UserID4, recv[1] + "/" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_W1_M4.jpg");
                        if (Globalspace._UserID1 != -1) ScreenShot(Globalspace._UserID1, recv[1] + "/" + recv[2] + "/" + recv[1] + "-" + DateTime.Now.ToString("yyyyMMddHHmm") + "_W1_M1.jpg");
                        if (Globalspace._UserID2 != -1) ScreenShot(Globalspace._UserID2, recv[1] + "/" + recv[2] + "/" + recv[1] + "-" + DateTime.Now.ToString("yyyyMMddHHmm") + "_W1_M2.jpg");
                        if (Globalspace._UserID3 != -1) ScreenShot(Globalspace._UserID3, recv[1] + "/" + recv[2] + "/" + recv[1] + "-" + DateTime.Now.ToString("yyyyMMddHHmm") + "_W1_M3.jpg");
                        if (Globalspace._UserID4 != -1) ScreenShot(Globalspace._UserID4, recv[1] + "/" + recv[2] + "/" + recv[1] + "-" + DateTime.Now.ToString("yyyyMMddHHmm") + "_W1_M4.jpg");


                    }
                    if (recv[0] == "SecondWeighing")
                    {
                        //if (Globalspace._UserID1 != -1) ScreenShot(Globalspace._UserID1, recv[1] + "/" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_W2_M1.jpg");
                        //if (Globalspace._UserID2 != -1) ScreenShot(Globalspace._UserID2, recv[1] + "/" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_W2_M2.jpg");
                        //if (Globalspace._UserID3 != -1) ScreenShot(Globalspace._UserID3, recv[1] + "/" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_W2_M3.jpg");
                        //if (Globalspace._UserID4 != -1) ScreenShot(Globalspace._UserID4, recv[1] + "/" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_W2_M4.jpg");
                        if (Globalspace._UserID1 != -1) ScreenShot(Globalspace._UserID1, recv[1] + "/" + recv[2] + "/" + recv[1] + "-" + DateTime.Now.ToString("yyyyMMddHHmm") + "_W2_M1.jpg");
                        if (Globalspace._UserID2 != -1) ScreenShot(Globalspace._UserID2, recv[1] + "/" + recv[2] + "/" + recv[1] + "-" + DateTime.Now.ToString("yyyyMMddHHmm") + "_W2_M2.jpg");
                        if (Globalspace._UserID3 != -1) ScreenShot(Globalspace._UserID3, recv[1] + "/" + recv[2] + "/" + recv[1] + "-" + DateTime.Now.ToString("yyyyMMddHHmm") + "_W2_M3.jpg");
                        if (Globalspace._UserID4 != -1) ScreenShot(Globalspace._UserID4, recv[1] + "/" + recv[2] + "/" + recv[1] + "-" + DateTime.Now.ToString("yyyyMMddHHmm") + "_W2_M4.jpg");

                    }
                    if (recv[0] == "OnceWeighing")
                    {
                        //if (Globalspace._UserID1 != -1) ScreenShot(Globalspace._UserID1, recv[1] + "/" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_W_M1.jpg");
                        //if (Globalspace._UserID2 != -1) ScreenShot(Globalspace._UserID2, recv[1] + "/" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_W_M2.jpg");
                        //if (Globalspace._UserID3 != -1) ScreenShot(Globalspace._UserID3, recv[1] + "/" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_W_M3.jpg");
                        //if (Globalspace._UserID4 != -1) ScreenShot(Globalspace._UserID4, recv[1] + "/" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_W_M4.jpg");
                        if (Globalspace._UserID1 != -1) ScreenShot(Globalspace._UserID1, recv[1] + "/" + recv[2] + "/" + recv[1] + "-" + DateTime.Now.ToString("yyyyMMddHHmm") + "_W_M1.jpg");
                        if (Globalspace._UserID2 != -1) ScreenShot(Globalspace._UserID2, recv[1] + "/" + recv[2] + "/" + recv[1] + "-" + DateTime.Now.ToString("yyyyMMddHHmm") + "_W_M2.jpg");
                        if (Globalspace._UserID3 != -1) ScreenShot(Globalspace._UserID3, recv[1] + "/" + recv[2] + "/" + recv[1] + "-" + DateTime.Now.ToString("yyyyMMddHHmm") + "_W_M3.jpg");
                        if (Globalspace._UserID4 != -1) ScreenShot(Globalspace._UserID4, recv[1] + "/" + recv[2] + "/" + recv[1] + "-" + DateTime.Now.ToString("yyyyMMddHHmm") + "_W_M4.jpg");
                    }

                    if (recv[0] == "StartWeighing")
                    {
                        if (Globalspace._UserID1 != -1)
                        {
                            CHCNetSDK.NET_DVR_MakeKeyFrame(Globalspace._UserID1, 1);
                            //开始录像 Start recording
                            //var sVideoFileName = "Snap/Temp/" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".mp4";
                            //var sVideoFileName = MonitorSavePath+DateTime.Now.ToString("yyyyMMddHHmmss") + ".mp4";

                            var videoPath = MonitorSavePath + "/" + recv[1] + "/" + recv[2];
                            if (!System.IO.Directory.Exists(videoPath))
                            {
                                System.IO.Directory.CreateDirectory(videoPath);
                            }

                            var sVideoFileName = videoPath + "/" + recv[1] + "-" + DateTime.Now.ToString("yyyyMMddHHmm") + ".mp4";
                            sVideoFileName = sVideoFileName.Replace("/","\\");

                            if (!CHCNetSDK.NET_DVR_SaveRealData(Globalspace._UserID1, sVideoFileName))
                            {
                                var iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                                string str = "NET_DVR_SaveRealData failed, error code= " + iLastErr;
                                log.Error(str);
                            }
                            else
                            {
                                var delayStopTimer = new DispatcherTimer { Interval = TimeSpan.FromSeconds(10) };
                                delayStopTimer.Start();
                                delayStopTimer.Tick += (sender, args) =>
                                {
                                    delayStopTimer.Stop();

                                //停止录像 Stop recording
                                if (!CHCNetSDK.NET_DVR_StopSaveRealData(Globalspace._UserID1))
                                    {
                                        var iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                                        string str = "NET_DVR_StopSaveRealData failed, error code= " + iLastErr;
                                        log.Error(str);
                                    }
                                    else
                                    {
                                        string str = "saved file is " + sVideoFileName;
                                        log.Info(str);
                                    }
                                };
                            }
                        }
                        else
                        {
                            log.Info("录像失败，相机未打开");
                        }
                    }
                }

            }
            catch (Exception e)
            {
                log.Info($"WndProc error:{e.Message}");

            }
            return hwnd;
        }

        private void ScreenShot(int userID, string sJpegPicFileName)
        {
            //sJpegPicFileName = "Snap/" + sJpegPicFileName;
            //sJpegPicFileName = MonitorSavePath+ sJpegPicFileName;
            sJpegPicFileName = sJpegPicFileName.Replace("/","\\");
            sJpegPicFileName = PictureSavePath + sJpegPicFileName;

            CHCNetSDK.NET_DVR_JPEGPARA lpJpegPara = new CHCNetSDK.NET_DVR_JPEGPARA
            {
                wPicQuality = 0,
                wPicSize = 0xff
            };

            CHCNetSDK.NET_DVR_CaptureJPEGPicture(userID, 1, ref lpJpegPara, sJpegPicFileName);
        }
    }
}
